import React from "react";
import styled from "styled-components";
import propTypes from "prop-types";

import {MenuListItem} from "react95";

type Props = Partial<Pick<React.ComponentProps<typeof SList>, "horizontalAlign" | "verticalAlign">> & {
    trigger: React.ComponentType<any>;
    items: {
        onClick: () => void;
        label: string;
    }[];
};

const Menu = ({trigger, items}: Props) => {
    const [open, setOpen] = React.useState(false);

    function toggle() {
        setOpen(!open);
    }

    function handleClose() {
        setOpen(false);
    }

    const TriggerElement = trigger;
    const listItems = items.map((item, i) => (
        <MenuListItem key={i} onClick={item.onClick} size="lg">
            {item.label}
        </MenuListItem>
    ));
    return (
        <div style={{position: "relative", display: "inline-block"}}>
            {open && (
                <SList
                    onClick={handleClose}
                    style={{zIndex: 1}}
                >
                    {listItems}
                </SList>
            )}
            <TriggerElement onClick={toggle} active={open}/>
        </div>
    );
};

Menu.propTypes = {
    trigger: propTypes.element,
    items: propTypes.arrayOf(
        propTypes.shape({
            label: propTypes.string,
            onClick: propTypes.func,
        })
    ),
};

export default Menu;

const SList = styled(MenuListItem)`
  position: absolute;
  top: 100%;
  right: 0;
`;
